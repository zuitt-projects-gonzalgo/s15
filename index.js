//alert("Hello");

//assignment operators

//basic assignment operator (=)
	//it allows to assign a value to a variable
	let variable = "initial value";

//mathematical operators (addition (+), subtraction (-), multiplication (*), division (/), modulo (%))
let num1 = 5;
let num2 = 10;
let num3 = 4;
let num4 = 40;

	//addition assignment operator (+=)
	//left operand is the vaiable or the value of the left side of the operator
	//right operand is the variable or the value of the right side of the operator
	//num1 = num1 + num4 (reasigned the value of num1 with the result of num1 + num4)
	num1 += num4;
	console.log(num1);
		//result: 45
	num1 += 55;
	console.log(num1);
		//result: 100

	let string1 = "Boston";
	let string2 = "Celtics";
	string1 += string2;
	console.log(string1);

	//subtraction assignment operator
	num1 -= num2;
	console.log(num1);
		//result: 90
	num1 -= num4;
	console.log(num1);
		//result: 50
	num1 -= 10;
	console.log(num1);
		//result: 40
	num1 -= string1;
	console.log(num1);
		//result: NaN (Not a Number)
	string1 -= string2;
	console.log(string1);
		//result: NaN

	//multiplication assignment operator
	num2 *= num3;
	console.log(num2);
		//result: 40
	num2 *= 5;
	console.log(num2);
		//result: 200

	//division assignment operator (/=)
	num4 /= num3;
	console.log(num4);
		//result: 10
	num4 /= 2;
	console.log(num4);
		//result: 5

	//modulo operator (%)
	let y = 15;
	let x = 2;
	console.log(y % x);

//mathematical operations - we follow MDAS
let mdasResult = 1 + 2 - 3 * 4 / 5;
console.log(mdasResult);
	//result = 0.6

//PEMDAS
let pemdasResult = 1 + (2 - 3) * (4 / 5);
console.log(pemdasResult);
	//result = .2

//2 kinds of incrementation: prefix and postfix
let z = 1;
	//pre-fix increment
++z;
console.log(z);
	//result = 2
	//the value of z was addded with 1 and is immediately returned

	//post-fix increment
z++;
console.log(z);
	//result = 3
console.log(z++);
	//result = 3
console.log(z);

	//pre-fix decrement
	console.log(--z);
		//result: 3
	console.log(z--);
		//result: 3
	console.log(z);
		//result: 2

//comparison operators
	// this is used to compare the valus of left and right operand
	//note that comparison operators returns a boolean

	//equality or loose equality operators (==)
	console.log(1 == 1);
		//result: true

	//we can also save the result of a comparison in a variable
	let isSame = 55 == 55;
	console.log(isSame);
		//result: true
	console.log(1 == "1");
		//result: true
		//note: loose equality prioritize the sameness of the value
		//force coercion is done before comparison. force coercion/forced conversion - JS forcibly changing the data type of the operands
	console.log(0 == false);
		//result: true
	console.log(1 == true);
		//result: true
	console.log("1" == 1);
		//result: true
	console.log(true == "true");
		//result: false
	console.log(true == "1");
		//result: true

	//strict equality operator (===)
	console.log(true === 1);
		//result: false
		//note: checks both the value and the data type
	console.log("lisa" === "Lisa");
		//result: false

//inequality operators
	//loose inequality operators (!=)
	console.log("1" != 1);
		//result: false
	console.log("Rose" != "Jennie");
		//result: true

	//strict inequality operators (!==)
	console.log("5" !== 5);
		//result: true


//mini-activity
let name1 = "Juan";
let name2 = "Shane";
let name3 = "Peter";
let name4 = "Jack";

let number = 50;
let number2 = 60;
let numString1 = "50";
let numString2 = "60";

console.log(numString1 == number);
console.log(numString1 === number);
console.log(numString1 != number);
console.log(name4 !== name3);
console.log(name1 == "juan");
console.log(name1 === "Juan");

//relational comparison operators
	//a comparison operator which will check the relationship between operands
	let q = 500;
	let r = 700;
	let w = 8000;
	let numString3 = "5500";

	//greater than (>)
	console.log(q > r);
		//result: false
	console.log(w > r);
		//result: true

	//less than (<)
	console.log(w < q);
		//result: false
	console.log(q < 1000);
		//result: true
	console.log(numString3 < 6000);
		//result: true
	console.log(numString3 < "Jose");
		//result: true - this is erratic

	//greater than or equal to (>=)
	console.log(w >= 8000);
		//result: true
	console.log(r >= q);
		//result: true

	//less than or equal to (<=)
	console.log(q <= r);
		//result: true
	console.log(w <= q);
		//result: false

//logical operators 
	// and operator (&&)
		//both operands on the left and right or all operands added must be true or will result to true
	let isAdmin = false;
	let isRegistered = true;
	let isLegalAge = true;

	let authorization1 = isAdmin && isRegistered;
		//result: false
	let authorization2 = isRegistered && isLegalAge;
		//result: true

	let requiredLevel = 95;
	let requiredAge = 18;
	let authorization3 = isRegistered && requiredLevel === 25;
		//result: false
	
	let userName = "gamer2001";
	let userName2 = "shadow1991";
	let userAge = 15;
	let userAge2 = 30;

	let registration1 = userName.length > 8 && userAge >= requiredAge;
		//result: false

	//or operator (|| - double pipe)
		//or operator returns true if at least one of the operands is true
	let userLevel = 100;
	let userLevel2 = 65;

	let guildRequirement1 = isRegistered || userLevel >= requiredLevel || userAge >= requiredAge
		//result: true

	//not operator (!)
		//turns a boolean value into the opposite value

	console.log(!isRegistered);
		//result: false
	console.log(!isAdmin);
		//result: true

//if-else statements
	//if statement will run a block of code if the condition specified is true or results to true
	let userName3 = "crusader_1993";
	let userLevel3 = 25;
	let userAge3 = 20;

	if(userName3.length > 10) {
		console.log("Welcome to Game Online!")
	};

	if(userLevel3 >= requiredLevel) {
		console.log("You are qualified to join the guild!");
	};

	if(userName3 >= 10 && isRegistered && isAdmin) {
		console.log("Thank you for joining the admin!");
	};

	//else statement
		//will run if the condition given is false or results to false

	if(userName3.length >= 10 && userLevel3 <= 25 && userAge3 >= requiredAge) {
		console.log("Thank you for joining the Noob guild!")
	} else {
		console.log("You are too strong to be a noob :(")
	};

	if(userName3.length >= 10 && userLevel3 <= 25 && userAge3 >= requiredAge) {
		console.log("Welcome to Noob Guild");
	} else if(userLevel3 > 25) {
		console.log("You are too strong to be a noob");
	} else if(userAge3 < requiredAge) {
		console.log("You are too young to join the guild");
	} else if(userName3.length < 10) {
		console.log("Username is too short");
	} else {
		console.log("You cannot join");
	}

//if-else inside a function

function addNum(num1, num2) {
	if(typeof num1 === "number" && typeof num2 === "number") {
		console.log("Run only of both arguments passed are number types");
		console.log(num1 + num2);
	} else {
		console.log("One or both of the arguments are not numbers")
	}
}

addNum(5, 2);

function login(username, password) {
	if(typeof username === "string" && typeof password === "string") {
		console.log("Both arguments are string");
		if(username.length >= 8 && password.length >=8) {
			//alert("Thank you for logging in");
		} else if(username.length < 8) {
			//alert("username is too short");
		} else if(password.length < 8) {
			//alert("password is too short");
		}
	} else {
		console.log("One if the arguments is not a string type")
	}
}

login("bellTinker", "tinkerbell");

//switch statements
	//is an alternative to an if, else-if, else tree, where the data being evaluated of checked is of an expected input.
	//syntax:
	/*
		switch(expression/condition) {
			case value:
				statement;
				break;
			default:
				statement;
				break;
		}
	*/

let hero = "Hercules";
switch(hero) {
	case "Jose Rizal":
	console.log("National Hero of the Philippines");
	break;

	case "George Washington":
	console.log("Hero of American Revolution");
	break;

	case "Hercules":
	console.log("Legendary hero of the Greeks");
}

function roleChecker(role) {
	switch(role) {
		case "Admin":
		console.log("Welcome Admin");
		break;

		case "User":
		console.log("Welcome User");
		break;

		case "Guest":
		console.log("Welcome Guest");
		break;

		default:
		console.log("Invalid");
	}
}

roleChecker("admin");

function gradeEvaluator(grade) {
	if(grade >= 90) {
		return "A";
	} else if(grade >= 80) {
		return "B";
	} else if(grade >= 71) {
		return "C";
	} else if(grade <= 70) {
		return "F";
	} else {
		return "Invalid Grade";
	}
}

let letterDistinction = gradeEvaluator(85);
console.log(letterDistinction);
	//result: B

//Ternary Operator
	//a shorthand way of writing if-else statements
	/*
		Syntax:
		condition ? if-statement : else-statement
	*/

let price = 5000;
price > 1000 ? console.log("Price is over 1000") : console.log("Price is less than 1000");

let villain = "Harvey Dent";
//villain === "Harvey Dent" ? console.log("You were supposed to be the chosen one");
villain === "Two Face"
? console.log("You lived long enough to be a villain")
: console.log("Not quite villainous yet");
//note: ternary operators are not meant for complex if-else trees. However, the main advantage of ternary operator is not because it's short, rather ternary operation implicitly returns or it can return without the return keyword

let robin1 = "Dick Grayson";
let currentRobin = "Tim Drake";

let isFirstRobin = currentRobin === robin1 ? true : false;
console.log(isFirstRobin);

	//else-if with ternary operator
	let a = 7;
	a === 5
	? console.log("A")
	: console.log(a === 10 ? console.log("A is 10") : console.log("A is not 5 or 10"));