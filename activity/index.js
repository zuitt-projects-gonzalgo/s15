function oddEventChecker(num) {
	let modulo = 0;
	if(typeof num === "number") {
		if(num % 2 == 0) {
			console.log("The Number is Even");
		} else {
			console.log("The Number is Odd");
		}
	} else {
		alert("Invalid Input");
	}
}

function budgetChecker(budget) {
	if(typeof budget === "number") {
		if(budget > 40000) {
			console.log("You are over the Budget");
		} else {
			console.log("You have resources left");
		}
	} else {
		alert("Invalid Input");
	}
}